package convert

import (
	"reflect"
	"strconv"
)

func (defaultFuncs) ToFloats(this *Convert) {
	outs := []interface{}{
		reflect.TypeOf(float32(0)),
		reflect.TypeOf(float64(0)),
		reflect.Float32,
		reflect.Float64,
	}
	inString := []interface{}{
		reflect.TypeOf(""),
		reflect.String,
	}
	inBool := []interface{}{
		reflect.Bool,
	}

	//> Float <- String
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		f, err := strconv.ParseFloat(in.String(), 64)
		if err != nil {
			return err
		}
		out.SetFloat(f)
		return nil
	}, outs, inString, "0")

	//> Float <- Bool
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		if in.Bool() {
			out.SetFloat(1)
		} else {
			out.SetFloat(0)
		}
		return nil
	}, outs, inBool, false)
}
