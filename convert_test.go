package convert

import (
	"reflect"
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

type _testStruct1 struct {
	Sp *string
	S  string
	I  *int
}

func TestConvert(t *testing.T) {

	Convey("Golang reflection laws", t, func() {
		var obj interface{}
		newStr := reflect.New(reflect.TypeOf(""))
		newStr.Elem().Set(reflect.ValueOf("str1"))

		Convey("1", func() {
			ts := _testStruct1{}
			Println(reflect.ValueOf(&ts).Elem().FieldByName("Sp").Type().Elem())
			So(reflect.ValueOf(&ts).Elem().FieldByName("Sp").Elem().IsValid(), ShouldBeFalse)
			So(func() { reflect.ValueOf(&ts).Elem().FieldByName("Sp").Elem().IsValid() }, ShouldNotPanic)
		})

		Convey("nil", func() {
			So(obj, ShouldBeNil)
			So(reflect.ValueOf(obj).IsValid(), ShouldBeFalse)
			So(func() { reflect.ValueOf(obj).IsNil() }, ShouldPanic)
			So(reflect.ValueOf(obj).Kind(), ShouldEqual, reflect.Invalid)
		})

		//> nil ptr
		Convey("nil ptr", func() {
			var ptr *string
			So(reflect.ValueOf(ptr).IsValid(), ShouldBeTrue)
			So(reflect.ValueOf(ptr).IsNil(), ShouldBeTrue)
			Convey("Set * on &", func() {
				So(func() { reflect.ValueOf(ptr).Elem().Set(newStr.Elem()) }, ShouldPanic)
			})
			Convey("Set * on &&", func() {
				So(func() { reflect.ValueOf(&ptr).Elem().Elem().Set(newStr.Elem()) }, ShouldPanic)
			})
			Convey("Set & on &&", func() {
				So(func() { reflect.ValueOf(&ptr).Elem().Set(newStr) }, ShouldNotPanic)
			})
		})

		Convey("struct field", func() {
			s := &_testStruct1{}
			obj = s

			Convey("1", func() {
				So(func() { reflect.ValueOf(obj).Elem().FieldByName("Sp").Set(newStr) }, ShouldNotPanic)
				So(*s.Sp, ShouldEqual, "str1")
			})

			Convey("2", func() {
				So(func() { reflect.ValueOf(obj).Elem().FieldByName("S").Set(newStr.Elem()) }, ShouldNotPanic)
				So(s.S, ShouldEqual, "str1")
			})

			Convey("3", func() {
				So(func() { reflect.ValueOf(*s).FieldByName("S").Set(newStr.Elem()) }, ShouldPanic)
			})
		})

	})

	Convey("???", t, func() {
		conv := New(DefaultInit)

		var in = reflect.ValueOf(23)
		var out string
		err := conv.Do(&out, in)
		So(err, ShouldBeNil)
		So(out, ShouldEqual, "23")
	})

	Convey("String <= Basic", t, func() {
		conv := New(DefaultInit)

		var in = 23
		var out string
		err := conv.Do(&out, in)
		So(err, ShouldBeNil)
		So(out, ShouldEqual, "23")
	})

	Convey("String <= Basic (default value)", t, func() {
		conv := New(DefaultInit)

		var in *int
		var out string
		err := conv.Do(&out, in)
		So(err, ShouldBeNil)
		So(out, ShouldEqual, "0")
	})

	SkipConvey("String <= Basic (nil value)", t, func() {
		conv := New(DefaultInit)

		var out string
		err := conv.Do(&out, nil)
		So(err, ShouldBeNil)
		So(out, ShouldEqual, "")
	})

	SkipConvey("String <= Basic (nil value, reflect.Value)", t, func() {
		conv := New(DefaultInit)

		var out string
		err := conv.Do(&out, reflect.ValueOf(nil))
		So(err, ShouldBeNil)
		So(out, ShouldEqual, "")
	})

	Convey("String <= Map", t, func() {
		conv := New(DefaultInit)

		var in = map[string]interface{}{
			"n": 46,
		}
		var out string
		err := conv.Do(&out, in)
		So(err, ShouldBeNil)
		So(out, ShouldEqual, "{\n  \"n\": 46\n}")
	})

	Convey("Pointer value creation", t, func() {
		conv := New(DefaultInit)

		var in = 23
		var out *string
		So(out, ShouldBeNil)
		err := conv.Do(&out, in)
		So(err, ShouldBeNil)
		So(out, ShouldNotBeNil)
		So(*out, ShouldEqual, "23")
	})

	Convey("Pointer value creation 2 (reflect.Value)", t, func() {
		conv := New(DefaultInit)
		var out *string

		var in = 23
		var outv = reflect.ValueOf(&out)
		So(out, ShouldBeNil)
		err := conv.Do(outv.Elem(), in)
		So(err, ShouldBeNil)
		So(out, ShouldNotBeNil)
		So(*out, ShouldEqual, "23")
	})

	SkipConvey("Pointer value creation 2/2 (reflect.Value)", t, func() {
		conv := New(DefaultInit)
		var out *string

		var in = 23
		var outv = reflect.ValueOf(out)
		So(out, ShouldBeNil)
		err := conv.Do(outv, in)
		So(err, ShouldBeNil)
		So(out, ShouldNotBeNil)
		So(*out, ShouldEqual, "23")
	})

	Convey("Output to reflect.Value", t, func() {
		conv := New(DefaultInit)
		var out *string

		var in = reflect.ValueOf(23)
		var outv = reflect.ValueOf(&out)
		So(out, ShouldBeNil)
		err := conv.Do(outv, in)
		So(err, ShouldBeNil)
		So(out, ShouldNotBeNil)
		So(*out, ShouldEqual, "23")
	})

	Convey("Output to *reflect.Value (produces error)", t, func() {
		conv := New(DefaultInit)
		var out *string

		var in = reflect.ValueOf(23)
		var outv = reflect.ValueOf(&out)
		So(out, ShouldBeNil)
		err := conv.Do(&outv, in)
		So(err, ShouldNotBeNil)
	})
}

//func BenchmarkAssignable(b *testing.B) {
//	s := "#12:375"
//	var rid RID
//	b.StartTimer()
//	for i := 0; i < b.N; i++ {
//		err := conv.Do(&rid, s)
//		if err != nil || rid.Cluster != 12 || rid.Position != 375 {
//			b.Error(err)
//			b.FailNow()
//		}
//	}
//	b.StopTimer()
//}

//func BenchmarkConvertable(b *testing.B) {
//	s := "#12:375"
//	var rid RID
//	b.StartTimer()
//	for i := 0; i < b.N; i++ {
//		err := conv.Do(&rid, s)
//		if err != nil || rid.Cluster != 12 || rid.Position != 375 {
//			b.Error(err)
//			b.FailNow()
//		}
//	}
//	b.StopTimer()
//}
