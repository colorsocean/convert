package convert

import (
	"errors"
	"fmt"
	"reflect"
)

// todo: Recurse 'out' pointers
// todo: Out object creation
// todo: Replace convert calls to lower level calls when possible
// todo: Implement 'boost' function
// todo: When checking use the same order as when adding
// todo: Develop a case when custom float->string formatter is needed

type ConvertFunc func(conv *Convert, out reflect.Value, in reflect.Value) error
type InitFunc func(this *Convert)

type convstruct struct {
	fn ConvertFunc
	dv reflect.Value
}

type Convert struct {
	funcs map[interface{}]map[interface{}]convstruct
}

func New(inits ...InitFunc) Convert {
	conv := Convert{
		funcs: make(map[interface{}]map[interface{}]convstruct),
	}
	for _, init := range inits {
		init(&conv)
	}
	return conv
}

func (this *Convert) AddFunc(fn ConvertFunc, out_s, in_s, defaultIn interface{}) error {
	var outs []interface{}
	var ins []interface{}
	var ok bool

	if reflect.TypeOf(out_s).Kind() == reflect.Slice {
		outs, ok = out_s.([]interface{})
		if !ok {
			return errors.New("out_s must be either slice of interfaces or reflect.Type or reflect.Kind")
		}
	} else {
		outs = []interface{}{out_s}
	}

	if reflect.TypeOf(in_s).Kind() == reflect.Slice {
		ins, ok = in_s.([]interface{})
		if !ok {
			return errors.New("in_s must be either slice of interfaces or reflect.Type or reflect.Kind")
		}
	} else {
		ins = []interface{}{in_s}
	}

	for _, out := range outs {
		switch out.(type) {
		case reflect.Type:
		case reflect.Kind:
		default:
			return errors.New("out/in must be of type reflect.Type or reflect.Kind")
		}

		param2map, ok := this.funcs[out]
		if !ok {
			param2map = make(map[interface{}]convstruct)
			this.funcs[out] = param2map
		}
		for _, in := range ins {
			param2map[in] = convstruct{fn, reflect.ValueOf(defaultIn)}
		}
	}

	return nil
}

func (this *Convert) Get(t reflect.Type, in interface{}) (v reflect.Value, err error) {
	v = reflect.Indirect(reflect.New(t))
	err = this.Do(v, in)
	return
}

func (this *Convert) Do(out interface{}, in interface{}) (err error) {
	var vOut reflect.Value
	var vIn reflect.Value
	var tOut reflect.Type
	var tIn reflect.Type

	if out == nil {
		return errors.New("Please, provide a non-nil pointer in 'out'")
	}

	if vo, ok := out.(reflect.Value); ok {
		vOut = vo
		if vOut.Kind() != reflect.Ptr {
			if !vOut.CanSet() {
				return errors.New("Please, provide pointer/settable value in 'out'")
			}
		}
	} else {
		vOut = reflect.ValueOf(out)
		if vOut.Kind() != reflect.Ptr {
			return errors.New("Please, provide pointer in 'out'")
		}
	}
	tOut = vOut.Type()

	if vi, ok := in.(reflect.Value); ok {
		vIn = vi
		if vIn.IsValid() {
			tIn = vIn.Type()
		} else {
			return errors.New("Can not determine type of zero-value in 'in'")
		}
	} else {
		vIn = reflect.ValueOf(in)
		tIn = reflect.TypeOf(in)
	}

	if tIn.Kind() == reflect.Invalid {
		vIn = reflect.New(tOut).Elem()
		tIn = vIn.Type()
	}

	return this.convertChecks(vOut, tOut, vIn, tIn)
}

func (this *Convert) convertChecks(vOut reflect.Value, tOut reflect.Type, vIn reflect.Value, tIn reflect.Type) error {
	if vIn.IsValid() && vIn.Type().Kind() == reflect.Ptr {
		return this.convertChecks(vOut, tOut, vIn.Elem(), tIn.Elem())
	}

	if tOut.Kind() == reflect.Ptr {
		//> Create vOut in case of nil-pointer
		if tOut.Elem().Kind() != reflect.Ptr {
			if vOut.IsNil() {
				vOut.Set(reflect.New(tOut.Elem()))
			}
		}

		return this.convertChecks(vOut.Elem(), tOut.Elem(), vIn, tIn)
	}

	//> Ok, continue

	if vIn.IsValid() {
		//> Only assign if possible
		if tIn.AssignableTo(tOut) {
			vOut.Set(vIn)
			return nil
		}

		// hack: Lead to performance degradation
		{
			kOut := tOut.Kind()
			kIn := tIn.Kind()
			//> Disable int=>string conversion
			if !(kOut == reflect.String &&
				(kIn == reflect.Int ||
					kIn == reflect.Int8 ||
					kIn == reflect.Int16 ||
					kIn == reflect.Int32 ||
					kIn == reflect.Int64 ||
					kIn == reflect.Uint ||
					kIn == reflect.Uint8 ||
					kIn == reflect.Uint16 ||
					kIn == reflect.Uint32 ||
					kIn == reflect.Uint64)) {
				//> Or convert if possible
				if tIn.ConvertibleTo(tOut) {
					vOut.Set(vIn.Convert(tOut))
					return nil
				}
			}
		}
	}

	return this.convertMain(vOut, tOut, vIn, tIn)
}

func (this *Convert) convertMain(vOut reflect.Value, tOut reflect.Type, vIn reflect.Value, tIn reflect.Type) (err error) {

	funcmap, ok := this.funcs[tOut]
	if !ok {
		funcmap, ok = this.funcs[tOut.Kind()]
		if !ok {
			return errors.New(fmt.Sprintln("Converter for Out parameter is not registered", tOut, tOut.Kind()))
		}
	}

	cs, ok := funcmap[tIn]
	if !ok {
		cs, ok = funcmap[tIn.Kind()]
		if !ok {
			return errors.New(fmt.Sprintln("Converter for In parameter is not registered. out:", tOut, tOut.Kind(), "in:", tIn, tIn.Kind()))
		}
	}

	defer func() {
		if rec := recover(); rec != nil {
			err = errors.New(fmt.Sprint(rec))
			fmt.Println("#!!!!#", err)
		}
	}()

	if vIn.IsValid() {
		return cs.fn(this, vOut, vIn)
	} else {
		return cs.fn(this, vOut, cs.dv)
	}
}
