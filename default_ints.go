package convert

import (
	"reflect"
	"strconv"
)

func (defaultFuncs) ToInts(this *Convert) {
	outInts := []interface{}{
		reflect.TypeOf(int(0)),
		reflect.TypeOf(int8(0)),
		reflect.TypeOf(int16(0)),
		reflect.TypeOf(int32(0)),
		reflect.TypeOf(int64(0)),
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
	}
	outUInts := []interface{}{
		reflect.TypeOf(uint(0)),
		reflect.TypeOf(uint8(0)),
		reflect.TypeOf(uint16(0)),
		reflect.TypeOf(uint32(0)),
		reflect.TypeOf(uint64(0)),
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
	}
	inString := []interface{}{
		reflect.TypeOf(""),
		reflect.String,
	}
	inBool := []interface{}{
		reflect.TypeOf(bool(false)),
		reflect.Bool,
	}

	//> Ints <- String
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		n, err := strconv.ParseInt(in.String(), 10, 64)
		if err != nil {
			return err
		}
		out.SetInt(n)
		return nil
	}, outInts, inString, "0")

	//> UInts <- String
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		n, err := strconv.ParseUint(in.String(), 10, 64)
		if err != nil {
			return err
		}
		out.SetUint(n)
		return nil
	}, outUInts, inString, "0")

	//> Ints <- Bool
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		if in.Bool() {
			out.SetInt(1)
		} else {
			out.SetInt(0)
		}
		return nil
	}, outInts, inBool, false)

	//> UInts <- Bool
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		if in.Bool() {
			out.SetUint(1)
		} else {
			out.SetUint(0)
		}
		return nil
	}, outUInts, inBool, false)
}
