package convert

import "reflect"

func (defaultFuncs) ToBool(this *Convert) {
	outs := []interface{}{
		reflect.TypeOf(bool(false)),
		reflect.Bool,
	}
	inInts := []interface{}{
		reflect.TypeOf(int(0)),
		reflect.TypeOf(int8(0)),
		reflect.TypeOf(int16(0)),
		reflect.TypeOf(int32(0)),
		reflect.TypeOf(int64(0)),
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
	}
	inUInts := []interface{}{
		reflect.TypeOf(uint(0)),
		reflect.TypeOf(uint8(0)),
		reflect.TypeOf(uint16(0)),
		reflect.TypeOf(uint32(0)),
		reflect.TypeOf(uint64(0)),
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
	}
	inFloats := []interface{}{
		reflect.TypeOf(float32(0)),
		reflect.TypeOf(float64(0)),
		reflect.Float32,
		reflect.Float64,
	}
	inString := []interface{}{
		reflect.TypeOf(""),
		reflect.String,
	}

	//> Bool <- Ints
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		if in.Int() == 0 {
			out.SetBool(false)
		} else {
			out.SetBool(true)
		}
		return nil
	}, outs, inInts, int(0))

	//> Bool <- UInts
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		if in.Uint() == 0 {
			out.SetBool(false)
		} else {
			out.SetBool(true)
		}
		return nil
	}, outs, inUInts, uint(0))

	//> Bool <- Floats
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		if in.Float() == 0 {
			out.SetBool(false)
		} else {
			out.SetBool(true)
		}
		return nil
	}, outs, inFloats, float64(0))

	//> Bool <- Strings
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		if in.String() == "true" {
			out.SetBool(true)
		} else {
			out.SetBool(false)
		}
		return nil
	}, outs, inString, string("false"))
}
