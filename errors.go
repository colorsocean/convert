package convert

const (
	ErrUnsupportedType    = "Unsupported type"
	ErrNotImplemented     = "Feature not implemented"
	ErrWrongFuncPrototype = "Wrong convert func prototype"
)

func IsCriticalError(err error) bool {
	return !IsFormatError(err)
}

type FormatError struct {
	msg string
}

func newFormatError(msg string) error {
	return &FormatError{msg: msg}
}

func (this *FormatError) Error() string {
	return "Convert Format Error: " + this.msg
}

func IsFormatError(err error) bool {
	if _, ok := err.(*FormatError); ok {
		return true
	}
	return false
}

type InputError struct {
	msg string
}

func newInputError(msg string) error {
	return &InputError{msg: msg}
}

func (this *InputError) Error() string {
	return "Convert Input Error: " + this.msg
}

func IsInputError(err error) bool {
	if _, ok := err.(*InputError); ok {
		return true
	}
	return false
}
