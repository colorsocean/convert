package convert

import (
	"encoding/json"
	"fmt"
	"reflect"
)

func (defaultFuncs) ToString(this *Convert) {
	outString := []interface{}{
		reflect.TypeOf(""),
		reflect.String,
	}

	//> Basic types
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		out.Set(reflect.ValueOf(fmt.Sprint(in.Interface())))
		return nil
	}, outString, []interface{}{
		reflect.TypeOf(int(0)),
		reflect.TypeOf(int8(0)),
		reflect.TypeOf(int16(0)),
		reflect.TypeOf(int32(0)),
		reflect.TypeOf(int64(0)),
		reflect.TypeOf(uint(0)),
		reflect.TypeOf(uint8(0)),
		reflect.TypeOf(uint16(0)),
		reflect.TypeOf(uint32(0)),
		reflect.TypeOf(uint64(0)),
		reflect.TypeOf(float32(0)),
		reflect.TypeOf(float64(0)),
		reflect.TypeOf(bool(false)),
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Float32,
		reflect.Float64,
		reflect.Bool,
	}, 0)

	//> Map
	this.AddFunc(func(conv *Convert, out reflect.Value, in reflect.Value) error {
		data, err := json.MarshalIndent(in.Interface(), "", "  ")
		if err != nil {
			return err
		}

		var result string
		err = conv.Do(&result, data)
		if err != nil {
			return err
		}

		out.Set(reflect.ValueOf(result))
		return nil
	}, outString, []interface{}{
		reflect.TypeOf(make(map[string]interface{})),
		reflect.Map,
	}, make(map[string]interface{}))
}
