package convert

import "reflect"

var Default Convert = New(DefaultInit)

func Do(out interface{}, in interface{}) error {
	return Default.Do(out, in)
}

func Get(t reflect.Type, in interface{}) (v reflect.Value, err error) {
	return Default.Get(t, in)
}

func DefaultInit(this *Convert) {
	DefaultFuncs.ToString(this)
	DefaultFuncs.ToInts(this)
	DefaultFuncs.ToFloats(this)
	DefaultFuncs.ToBool(this)
}

var DefaultFuncs defaultFuncs

type defaultFuncs struct {
}
